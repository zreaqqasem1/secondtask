import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

void main() {
  runApp(HomePage());
}

class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(

        debugShowCheckedModeBanner: false,
        home: MyHomePage(),

    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  MyHomePageState createState() {
    return new MyHomePageState();
  }
}
class MyHomePageState extends State<MyHomePage> {

  TextEditingController controller = TextEditingController();
  TextEditingController controller1 = TextEditingController();
  TextEditingController controller2 = TextEditingController();
  TextEditingController controller3 = TextEditingController();
  TextEditingController controller4 = TextEditingController();

  int _value = 1;
  bool _validate = false;
  void dispose() {

    controller4.dispose();
    super.dispose();

  }

  Widget build(BuildContext context) {

   return Scaffold(
     backgroundColor: Colors.white,
     appBar: AppBar(
       toolbarHeight: 80,
       backgroundColor: Colors.white,
       shadowColor: Colors.black45,
       title: Text('CheckOut',
         style: TextStyle(
           color: Colors.black45,
           fontFamily: 'DDD' ,
           fontWeight: FontWeight.bold,
         ),
       ),
       centerTitle:  true ,
     ),
       body: SingleChildScrollView(
         padding: EdgeInsets.all(15),
         child: Container(

           child: Column(
             children: [

               Container(
                 child: Column(
                   children: [

                     Spacer(),

                     Row(children: [
                       Text('Credit Card Type',
                         textAlign: TextAlign.start,
                         style: TextStyle(
                             color: Hexcolor('#9DE7EC'),
                           fontSize: 25,
                           fontFamily: 'DDD',
                           fontWeight: FontWeight.bold,

                         ),
                       ),
                     ],
                     ),
                     Spacer(),
                     Row(
                       crossAxisAlignment: CrossAxisAlignment.center,
                       children: <Widget>[
                       DropdownButton(
                          value: _value,
                          items: [
                          DropdownMenuItem(
                          child: Text("Visa"),
                            value: 1,

                        ),
                       DropdownMenuItem(
                       child: Text("Master Card"),
                        value: 2,
                      ),
                        DropdownMenuItem(
                         child: Text("Pay Pal"),
                          value: 3
               ),
             ],
             onChanged: (value) {
               setState(() {
                 _value = value;
               });
                }),
                 ],
                     ),
                   ],

                 ),

                 height: 140,
               ),

               Container(

                 child:   Column(

                   children: [

                     Spacer(),

                     Row(children: [
                       Text('Full Name On Card',
                         textAlign: TextAlign.start,
                         style: TextStyle(
                           color: Hexcolor('#9DE7EC'),
                           fontSize: 25,
                           fontFamily: 'DDD',
                           fontWeight: FontWeight.bold,

                         ),
                       ),
                     ],
                     ),
                     Spacer(),
                     TextField(
                       controller: controller,
                       decoration: InputDecoration(
                         labelText: 'EXAMPLE :  QASEM MOEEAN ZREAQ',
                         errorText: _validate ? 'Value Can\'t Be Empty' : null,
                       ),
                     ),
                   ],
                 ),
                 height: 140,
               ),

               Container(

                 height: 140,
                 child:   Column(

                   children: [
                     Spacer(),
                     Row(children: [
                       Text('Credit Card Numer',
                         textAlign: TextAlign.start,
                         style: TextStyle(
                           color: Hexcolor('#9DE7EC'),
                           fontSize: 25,
                           fontFamily: 'DDD',
                           fontWeight: FontWeight.bold,

                         ),
                       ),
                     ],
                     ),
                     Spacer(),
                     TextField(
                       keyboardType: TextInputType.number,
                       controller: controller1,
                       decoration: InputDecoration(
                         labelText: 'EX : 0000  0000 0000 0000',
                         errorText: _validate ? 'Value Can\'t Be Empty' : null,
                       ),
                     ),
                   ],
                 ),
               ),
               Container(
                 height: 140,
                 child:   Column(
                   children: [
                     Expanded(child: Row(children: [
                       Text('Expiration Date',
                         style: TextStyle(
                           color: Hexcolor('#9DE7EC'),
                           fontSize: 25,
                           fontFamily: 'DDD',
                           fontWeight: FontWeight.bold,

                         ),
                       ),
                       Spacer(),
                       Spacer(),
                       Spacer(),
                       Text('CVV Code',
                         style: TextStyle(
                           color: Hexcolor('#9DE7EC'),
                           fontSize: 25,
                           fontFamily: 'DDD',
                           fontWeight: FontWeight.bold,

                         ),
                       ),
                       Spacer(),
                     ],
                     ),
                     ),
                   ],
                 ),
               ),
               Container(
                 height: 140,
                 child:   Column(
                   children: [
                     Row(children: [
                       Flexible(
                         child:
                         TextField(
                           keyboardType: TextInputType.number,
                           controller: controller2,
                           decoration: InputDecoration(
                             labelText: 'MM',
                             errorText: _validate ? 'Value Can\'t Be Empty' : null,
                           ),
                         ),
                       ),
                       Container(
                         width: 7,
                       ),
                       Flexible(
                         child:
                         TextField(
                           keyboardType: TextInputType.number,
                           controller: controller3,
                           decoration: InputDecoration(
                             labelText: 'YY',
                             errorText: _validate ? 'Value Can\'t Be Empty' : null,
                           ),
                         ),
                       ),
                       Container(
                         width: 7,
                       ),
                       Flexible(
                         child:
                         TextField(
                           keyboardType: TextInputType.number,
                           controller: controller4,
                           decoration: InputDecoration(
                             labelText: 'EX: 000',
                             errorText: _validate ? 'Value Can\'t Be Empty' : null,
                           ),
                         ),
                       ),
                     ],
                     ),
                   ],
                 ),
               ),
               Container(
                 child: ButtonTheme(
                   minWidth: 400.0,
                   height: 50.0,
                   child: RaisedButton(
                     color: Hexcolor('57B3F6'),
                     onPressed: () {
                       setState(() {
                         controller4.text.isEmpty ? _validate = true : _validate = false;

                       });
                     },
                     child: Text("Buy Now ",
                       style: TextStyle(
                         fontSize: 25,
                         fontFamily: 'DDD',
                           fontWeight: FontWeight.bold,

                           color: Colors.white
                       ),
                     ),
                   ),
                 ),
               ),
             ],
           ),
         ),
    ),
   );
     // Single child scroll vie
  }
}